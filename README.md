# Usage

## Building the container
~~~sh
docker build -t test/godot-ci .
~~~

## Running the container
~~~sh
docker run -ti test/godot-ci
~~~

From there you can 
~~~sh
git clone $repo
git install lfs
git checkout $branch
git lfs push
...
~~~